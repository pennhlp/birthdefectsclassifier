# BirthDefectsClassifier

## Requirements
* Python 2.7
* joblib
* nltk (Porter Stemmer)
* pandas (tested on 0.23.4)
* scikit-learn (tested on 0.19.2)

## Data
Available in the Supplementary Information files of https://doi.org/10.1038/s41746-019-0170-5:

* birth_defects_training_data_14716.tsv [(Supplementary Data 1)](https://static-content.springer.com/esm/art%3A10.1038%2Fs41746-019-0170-5/MediaObjects/41746_2019_170_MOESM1_ESM.txt)
* birth_defects_development_data_3681.tsv [(Supplementary Data 2)](https://static-content.springer.com/esm/art%3A10.1038%2Fs41746-019-0170-5/MediaObjects/41746_2019_170_MOESM2_ESM.txt)
* PennLexiconofBirthDefects_v2.txt [(Supplementary Data 3)](https://static-content.springer.com/esm/art%3A10.1038%2Fs41746-019-0170-5/MediaObjects/41746_2019_170_MOESM3_ESM.txt)
* LexicalVariants_PennLexiconofBirthDefects_v2.txt [(Supplementary Data 4)](https://static-content.springer.com/esm/art%3A10.1038%2Fs41746-019-0170-5/MediaObjects/41746_2019_170_MOESM4_ESM.txt)
* names.txt [(Supplementary Data 5)](https://static-content.springer.com/esm/art%3A10.1038%2Fs41746-019-0170-5/MediaObjects/41746_2019_170_MOESM5_ESM.txt)

Available from http://www.cs.cmu.edu/~ark/TweetNLP/clusters/50mpaths2:

* [50mpaths2.txt](http://www.cs.cmu.edu/~ark/TweetNLP/clusters/50mpaths2)

### Notes
* Use "birth_defects_training_data_14716.tsv" as the output filename when using [Supplementary Data 1](https://static-content.springer.com/esm/art%3A10.1038%2Fs41746-019-0170-5/MediaObjects/41746_2019_170_MOESM1_ESM.txt) as the input file to [download the tweets](https://bitbucket.org/pennhlp/twitter_data_download/src/master/) for training.
* Use "birth_defects_development_data_3681.tsv" as the output filename when using [Supplementary Data 2](https://static-content.springer.com/esm/art%3A10.1038%2Fs41746-019-0170-5/MediaObjects/41746_2019_170_MOESM2_ESM.txt) as the input file to [download the tweets](https://bitbucket.org/pennhlp/twitter_data_download/src/master/) for development.
* Change the filename of [Supplementary Data 3](https://static-content.springer.com/esm/art%3A10.1038%2Fs41746-019-0170-5/MediaObjects/41746_2019_170_MOESM3_ESM.txt) to "PennLexiconofBirthDefects_v2.txt".
* Change the filename of [Supplementary Data 4](https://static-content.springer.com/esm/art%3A10.1038%2Fs41746-019-0170-5/MediaObjects/41746_2019_170_MOESM4_ESM.txt) to "LexicalVariants_PennLexiconofBirthDefects_v2.txt".
* Change the filename of [Supplementary Data 5](https://static-content.springer.com/esm/art%3A10.1038%2Fs41746-019-0170-5/MediaObjects/41746_2019_170_MOESM5_ESM.txt) to "names.txt".