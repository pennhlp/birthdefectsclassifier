"""
    Script for performing birth defect classification from Twitter data

    @Authors: Abeed Sarker ...
"""

import csv, string
from nltk.stem.porter import *
from sklearn.feature_extraction.text import CountVectorizer
from joblib import dump
from collections import defaultdict

import sys
reload(sys)
import pandas as pd
import numpy as np
from sklearn import svm, model_selection
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support
import remove_malform

CHILD = ['child', 'baby', 'son', 'daughter', 'kid']
THIRD_PERSON_PRONOUN = ['he', 'his', 'him', 'she', 'her']
FIRST_PERSON_PRONOUN = ['my', 'our']

STEMMER = PorterStemmer()

sys.setdefaultencoding('utf8')

def getStructuralFeatures(processed_tweet):
    tweet_len_char = len(processed_tweet)
    tweet_len_word = len(processed_tweet.split())
    return tweet_len_char, tweet_len_word


def loadChildrenNames(filepath):
    names = []
    with open(filepath) as infile:
        for line in infile:
            names.append(string.strip(string.lower(line)))
    return names


NAMES_OF_CHILDREN = loadChildrenNames('./names.txt')


def preprocess_text(tweet_text):
    tweet_text = re.sub(r'&amp;', "and", tweet_text)
    tweet_text = re.sub('https?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*(),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+',
                        '_url_',
                        tweet_text)
    tweet_text = re.sub(r'pic.twitter.com/[A-Za-z0-9]+',
                        ' _url_',
                        tweet_text)
    tweet_text = re.sub(r'(@[A-Za-z0-9_]+)', ' _username_ ', tweet_text)
    tweet_text = remove_malform.remove_malform(tweet_text)
    tweet_text = re.sub(r'[^a-zA-Z_]', ' ', tweet_text)
    tweet_text = tweet_text.lower()
    tweet_text = re.sub(r'\s+', ' ', tweet_text)
    for tppro in THIRD_PERSON_PRONOUN:
        tweet_text = re.sub(r'\b' + tppro + r'\b', '_tppron_', tweet_text)
    for fppro in FIRST_PERSON_PRONOUN:
        tweet_text = re.sub(r'\b' + fppro + r'\b', '_fppron_', tweet_text)
    for ch in CHILD:
        tweet_text = re.sub(r'\b' + ch + r'\b', '_child_', tweet_text)
    for n in NAMES_OF_CHILDREN:
        tweet_text = re.sub(r'\b' + n + r'\b', '_name_', tweet_text)
    tweet_text = ' '.join(STEMMER.stem(w) for w in tweet_text.split())
    return tweet_text


def loadWordClusters():
    word_clusters = defaultdict(list)
    infile = open('./50mpaths2.txt')
    for line in infile:
        items = line.split()
        class_ = items[0]
        term = items[1]
        word_clusters[class_].append(term)
    return word_clusters


def getClusterFeatures(tweet, word_clusters):
        terms = tweet.split()
        cluster_string = ''
        for t in terms:
            for k in word_clusters.keys():
                if t in word_clusters[k]:
                    cluster_string += ' cl_'+ k + '_cl '
        return cluster_string


def loadDataAsDataFrame(f_path, w_clusters):
    datalist = []

    with open(f_path, 'rb') as csvfile:
        for row in csv.reader(csvfile, delimiter='\t'):
            instance_dict = {}
            # userid = '\"'+ row[0]+ '\"'
            tweetid = '\"' + row[2] + '\"'
            tweettext = preprocess_text(row[1].decode('utf-8'))
            len_char, len_word = getStructuralFeatures(tweettext)
            tweettext = '\"' + re.sub(r'\"', u'_qt_', tweettext) + '\"'
            # THE CLUSTER PART
            word_cluster = getClusterFeatures(tweettext, w_clusters)
            print word_cluster

            class_ = row[-1]

            instance_dict['id'] = tweetid
            instance_dict['class'] = class_
            instance_dict['text'] = tweettext
            instance_dict['w_cluster'] = word_cluster
            instance_dict['len_chars'] = [len_char]
            instance_dict['len_words'] = [len_word]

            datalist.append(instance_dict)
    return pd.DataFrame(datalist)


if __name__ == '__main__':

    w_clusters = loadWordClusters()
    training_data = loadDataAsDataFrame('./birth_defects_training_data_14716.tsv', w_clusters)
    dev_data = loadDataAsDataFrame('./birth_defects_development_data_3681.tsv', w_clusters)

    print len(training_data)
    training_data_texts = training_data['text']
    training_data_clusters = training_data['w_cluster']
    training_data_w_lens = training_data['len_words']
    training_data_c_lens = training_data['len_chars']

    dev_data_texts = dev_data['text']
    dev_data_clusters = dev_data['w_cluster']
    dev_data_w_lens = dev_data['len_words']
    dev_data_c_lens = dev_data['len_chars']

    print list(training_data_c_lens)
    print '-' * 5
    print len(training_data_texts)
    print len(training_data_clusters)
    print len(training_data_w_lens)
    print len(training_data_c_lens)
    print '-' * 5
    vectorizer = CountVectorizer(ngram_range=(1, 3),
                                 analyzer="word",
                                 tokenizer=None,
                                 preprocessor=None,
                                 max_features=10000)
    clustervectorizer = CountVectorizer(ngram_range=(1, 1),
                                        analyzer="word",
                                        tokenizer=None,
                                        preprocessor=None,
                                        max_features=3000)

    trained_data_vectors = vectorizer.fit_transform(training_data_texts).toarray()
    trained_cluster_vectors = clustervectorizer.fit_transform(training_data_clusters).toarray()
    trained_data_lens_vectors = map(list.__add__,
                                    list(training_data_w_lens),
                                    list(training_data_c_lens))

    training_data_combined_vectors = np.concatenate((trained_data_vectors, trained_cluster_vectors),
                                                    axis=1)
    training_data_combined_vectors = np.concatenate((training_data_combined_vectors, trained_data_lens_vectors),
                                                    axis=1)

    dev_data_vectors = vectorizer.transform(dev_data_texts).toarray()
    dev_cluster_vectors = clustervectorizer.transform(dev_data_clusters).toarray()
    dev_data_lens_vectors = map(list.__add__,
                                 list(dev_data_w_lens),
                                 list(dev_data_c_lens))

    dev_data_combined_vectors = np.concatenate((dev_data_vectors, dev_cluster_vectors),
                                               axis=1)
    dev_data_combined_vectors = np.concatenate((dev_data_combined_vectors, dev_data_lens_vectors),
                                               axis=1)

    c = 128
    w1 = 3.0
    w2 = 5.5
    folds = 10
    svm_classifier = svm.SVC(C=c,
                             class_weight={'1': w1,'2':w2,'3':1.0},
                             cache_size=40,
                             coef0=0.0,
                             degree=3,
                             gamma='auto',
                             kernel='rbf',
                             max_iter=-1,
                             probability=False,
                             random_state=None,
                             shrinking=True,
                             tol=0.001,
                             verbose=False)

    svm_classifier = svm_classifier.fit(training_data_combined_vectors, training_data['class'])
    dump(svm_classifier, './svm_classifier.joblib')
    dump(vectorizer,'./ngram_vectorizer.joblib')
    dump(clustervectorizer, './cluster_vectorizer.joblib')

    predictions = svm_classifier.predict(dev_data_combined_vectors)

    print 'c:', c, 'w1:', w1, 'w2:', w2,'||'
    print precision_recall_fscore_support(dev_data['class'], predictions)
    print confusion_matrix(dev_data['class'], predictions)
