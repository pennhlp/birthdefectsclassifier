"""
Export variant-incorporated query results about malformation
Ignore the tweets detected and exported earlier
"""


from collections import defaultdict
import re


stopwords = [
    'the',
    'this',
    'that',
    'these',
    'those',
    'a',
    'an',
    'his',
    'her',
    'of',
    'and',
    'with',
    'their',
    'between',
    'in',
    'on',
    'under',
    'both'
]


def read_lines(path_file):
    with open(path_file) as f:
        lines = [l.strip() for l in f.readlines() if re.search(r'\S', l)]
    return lines


def read_vars(path_file):
    """Read the variants compiled by Abeed and Ari"""
    dict_vars = dict()
    for l in read_lines(path_file):
        variants = [var.replace('_', ' ') for var in l.split()]
        init_form = variants[0]
        variants = list(set(variants))
        if len(variants) > 1:
            dict_vars[init_form] = variants
    return dict_vars


def insert_vars(kw, dict_vars):
    """Insert spelling variants into a keyphrase"""
    contained_keys = [w for w in kw.split() if w in dict_vars]
    for w in contained_keys:
        repl = '(?:' + '|'.join(dict_vars[w]) + ')'
        kw = re.sub(r'\b' + w + r'\b', repl, kw)
    return kw


def compose_stopwords(dict_vars):
    disj_stopwords = r'\b(?:' + '|'.join(stopwords) + r')\b'
    # Allow for ZERO occurrence of stopwords (may merged in a hashtag)
    regex_stopwords = r'(?:' + disj_stopwords + r'|\W)*'
    for stp_wd in set(stopwords) & set(dict_vars.keys()):
        regex_vars = '(?:' + '|'.join(dict_vars[stp_wd]) + ')'
        regex_stopwords = re.sub(r'(?<=[|:])' + stp_wd + r'(?=[|)])',
                                 regex_vars, regex_stopwords)
    return regex_stopwords


def compile_kw_dict():
    kw_to_regex, word_to_kw = dict(), defaultdict(set)
    # Used the list revised by Ari after the database query
    kws = read_lines('PennLexiconofBirthDefects_v2.txt')
    dict_vars = read_vars('LexicalVariants_PennLexiconofBirthDefects_v2.txt')
    regex_stopwords = compose_stopwords(dict_vars)
    for kw in kws:
        for w in kw.split():
            if w in dict_vars:
                for var in dict_vars[w]:
                    for wd in var.split():
                        word_to_kw[wd].add(kw)
            else:
                word_to_kw[w].add(kw)
        # NO leading or trailing r'\b' by default
        regex_vars = regex_stopwords.join(insert_vars(kw, dict_vars).split())
        kw_to_regex[kw] = regex_vars
    for kw in [
        'ntd',
        'chd',
        'hole heart',
        'vsd',
        'cdh',
        'hole mouth',
        'apsb',
        'avsd',
        'club foot',
        'dorv'
    ]:
        kw_to_regex[kw] = r'\b' + kw_to_regex[kw] + r'\b'
    for kw, regex in kw_to_regex.items():
        kw_to_regex[kw] = re.compile(regex, flags=re.I)
    return kw_to_regex, word_to_kw


KW_TO_REGEX, WORD_TO_KW = compile_kw_dict()


def remove_malform(text):
    overlap_kws = [val for key, val in WORD_TO_KW.iteritems() if key in text.lower()]
    if not overlap_kws:
        print 'Does not match any keyphrase!\n\t', text
        return text
    overlap_kws = set.union(*overlap_kws)
    malform_detected = False
    for kw in overlap_kws:
        compiled_regex = KW_TO_REGEX[kw]
        m = compiled_regex.search(text)
        if m:
            text = compiled_regex.sub(' _MALFORMATION_ ', text)
            #text = compiled_regex.sub(" ", text)
            # print kw, '\t', m.group(), '\t', text,
            malform_detected = True
    if not malform_detected:
        print 'Does not match any keyphrase!\n\t', text
    return text
